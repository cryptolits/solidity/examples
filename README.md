# Explorer

### [LP-pancakeswap](../../../-/tree/LP-pancakeswap)

---
Demonstration of how to create a liquidity pool at PancakeSwap via code.

### [Staking_pool](../../../-/tree/staking_pool)

---
Demonstration of staking pool implementation with complicated computation.

### [Nft metadata](../../../-/tree/create_metadata)

---
Demonstration and instruction of creating Nft collection with metadata.

### [Chainlink swap platform](../../../-/tree/swap-with-chainlink)

---
Swap platform that uses chainlink's AggregatorV3 to provide price feeds.

### [ERC1155](../../../-/tree/erc1155)

---
Realization of smart contract that mint Erc1155 tokens.

### [ICO](../../../-/tree/ico-contract)

---
Realization of the ICO smart contract
